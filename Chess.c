#include <stdio.h>
#include <stdlib.h>
#include "prtboard.h"

int Board[8][8] = {{W_Rook, W_Knight, W_Bishop, W_Queen, W_King, W_Bishop, W_Knight, W_Rook},
                   {W_Pawn, W_Pawn, W_Pawn, W_Pawn, W_Pawn, W_Pawn, W_Pawn, W_Pawn},
                   {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
                   {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
                   {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
                   {Empty, Empty, Empty, Empty, Empty, Empty, Empty, Empty},
                   {B_Pawn, B_Pawn, B_Pawn, B_Pawn, B_Pawn, B_Pawn, B_Pawn, B_Pawn},
                   {B_Rook, B_Knight, B_Bishop, B_Queen, B_King, B_Bishop, B_Knight, B_Rook}};

int main()
{
    printBoard(Board);
}

