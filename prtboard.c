#include <stdio.h>
#include "prtboard.h"

char abbre[15] = {' ', 'p', 'R', 'N', 'B', 'Q', 'K', 'p', 'R', 'N', 'B', 'Q', 'K'};

extern void printBoard(int board[8][8])
{
    printf("#################################    \n");
    for (int i = 7; i >= 0; i--)
    {
        printf("# %c # %c # %c # %c # %c # %c # %c # %c #  %d \n", abbre[board[i][0]], abbre[board[i][1]], abbre[board[i][2]], abbre[board[i][3]],
               abbre[board[i][4]], abbre[board[i][5]], abbre[board[i][6]], abbre[board[i][7]], i + 1);
        printf("#################################    \n");
    }
    printf("  a   b   c   d   e   f   g   h\n");
}